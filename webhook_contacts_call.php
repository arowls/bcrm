<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../wp-load.php');
require_once 'zohoAuthClass.php';
require 'zohoAPIClass.php';

$zohoObj = new zohoClass();

$zohoObj->check_access_token();
$access_token_time_remaining = $zohoObj->get_time_remaining($zohoObj->access_token_path);

// determine minutes left
if($access_token_time_remaining<=5){
    $zohoObj->generate_access_token();
}

// if(isset($_GET['code'])){
//     // read get vars (code) generate refresh and access token.  Store refresh token in file.
//     $this_response_arr = $zohoObj->generate_refresh_token();

//     // get refresh token from file
//     $refresh_token = base64_decode( file_get_contents( $zohoObj->refresh_token_path ) );

//     // check refresh token exists and is of expected length
//     if(strlen($refresh_token)==70){
//         echo '<h1>Yay! All went well.</h1>';
//         echo '<p><b>Refresh</b> Token successfully generated and stored.</p><pre>';
//         // print_r($this_response_arr);
//         // echo '</pre>';
//     }else{
//         echo '<h2>Oops! Something went wrong.</h2>';
//         echo '<p><b>Refresh</b> token was not regenerated.</p><pre>';
//         print_r($this_response_arr);
//         echo '</pre>';
//     }
// }

$access_token = $zohoObj->read_token($zohoObj->access_token_path);

$json_row = file_get_contents('php://input');

// define('AUTHTOKEN_BOOK', '80e6b535c6a96a172cb87b7ba04a0387');
define('BOOK_ORGID', '681163660');


$date = new DateTime();
$date = $date->format("m-d-Y h:i:s");
file_put_contents('contactlog.txt',"\n================> New Contact Added <==================\nOn ".$date."\n".print_r($json_row,TRUE), FILE_APPEND);
$json = json_decode($json_row);
$postData = (object) $json; 


class Zoho extends zohoAPIClass 
{
}

$zohoBokObj   = new Zoho($access_token);

if(count($postData) > 0){

// To find tax id from perticular Customer role, country, state and postcode
    $taxId="";
	$tax_exemption_id ="";
	//$current_user_data = get_userdata($postData->ID);
	$is_taxable = true;
	$userRoles = array('wholesale');
	$mauiZips = array(96793, 96790, 96788, 96784, 96779, 96770, 96768, 96767, 96763, 96761, 96757, 96753, 96748, 96742, 96733, 96732, 96729, 96713, 96708);

if($postData->shipping->country=="CA"){
        $taxId="1655091000000250005";
        $tax_authority_id ="1655091000000252003";
	}else if ($postData->shipping->country=="US" && $postData->shipping->state=="HI" && $postData->role =="wholesale") {
		$taxId="1655091000000082015";
        $tax_authority_id ="1655091000000070025";
	}else if (count(array_intersect($current_user_data->roles, $userRoles)) > 0)  {
		$taxId="1655091000000288019";
        $tax_authority_id ="1655091000000252003";
		$tax_exemption_id = "1655091000000079023";
		$is_taxable = false;
	}else if($postData->shipping->country=="US" && $postData->shipping->state=="FL"){
		$taxId="1655091000000085001";
		$tax_authority_id ="1655091000000082021";		
	}else if($postData->shipping->country=="US" && $postData->shipping->state=="HI" && (count(array_intersect($postData->shipping->postcode, $mauiZips)) > 0  )){
		$taxId="1655091000007884435";//Tax (HI-Maui)4.166
		$tax_authority_id="1655091000000070025";    
	}else if($postData->shipping->country=="US" && $postData->shipping->state=="HI"){
		$taxId="1655091000007884425";//Tax (HI-Oahu, Kauai, Hawaii) 4.712
		$tax_authority_id="1655091000000070025";
	}else if($postData->shipping->country=="US" && $postData->shipping->state=="NY"){
        $taxId="1655091000000250023";
        $tax_authority_id ="1655091000000252003";
		}
else{
		$taxId="1655091000000288019";
		$tax_authority_id ="1655091000000252003";
		//$tax_exemption_id = "1655091000000252007";
	    //$is_taxable = false;
}
    $contactID = $zohoBokObj->searchBookCRM($postData->email,'contacts',BOOK_ORGID);
    file_put_contents('contactlog.txt',"\nEmail Searched: ".$postData->email , FILE_APPEND);
    file_put_contents('contactlog.txt',"\n Found Contact Id: ".json_encode($contactID), FILE_APPEND);
    $contName = $postData->username;
    $attention_bill = $postData->billing->first_name.' '.$postData->billing->last_name;
    $attention_ship = $postData->shipping->first_name.' '.$postData->shipping->last_name;
    $companyName = ($postData->billing->company == "")?$postData->shipping->company:$postData->billing->company;
    $contactCreateAry = array(
        "contact_name" => trim($contName),
        "company_name" => $companyName,
        "billing_address" => array(
            "attention" => $attention_bill,
            "address" => $postData->billing->address_1,
            "street2" => $postData->billing->address_2,
            "city" => $postData->billing->city,
            "state" => $postData->billing->state,
            "zip" => $postData->billing->postcode,
            "country" => $postData->billing->country,
            "phone" => $postData->billing->phone
        ),
        "shipping_address" => array(
            "attention" => $attention_ship,
            "address" => $postData->shipping->address_1,
            "street2" => $postData->shipping->address_2,
            "city" => $postData->shipping->city,
            "state" => $postData->shipping->state,
            "zip" => $postData->shipping->postcode,
            "country" => $postData->shipping->country
        ),
        "contact_persons" => array(array(
                "first_name" => ($postData->first_name == "")?$postData->billing->first_name:$postData->first_name,
                "last_name" => ($postData->last_name == "")?$postData->billing->last_name:$postData->last_name,
                "email" => $postData->email,
                "phone" => $postData->billing->phone,
                "is_primary_contact" => true
        )),
			"tax_id"=>$taxId,
            "is_taxable"=> ($taxId=="" ? false : $is_taxable),
            "tax_authority_id"=>($taxId=="" ? "" :$tax_authority_id),
            "tax_exemption_id"=>$tax_exemption_id,
        
    );

  
    if (empty($contactID)){
        $contactData = $zohoBokObj->createContactRequest($contactCreateAry, 'contacts', BOOK_ORGID,true);
        $contactID=$contactData->contact->contact_id;

        file_put_contents('contactlog.txt',"\n\n New Contact Id: ".$contactID, FILE_APPEND);
        file_put_contents('contactlog.txt',"\n JSONstring for new: ".json_encode($contactCreateAry), FILE_APPEND);

    }
    else{

        $contactUpdateAry = array(
            "contact_name" => trim($contName),
            "company_name" => $companyName,
            "contact_type" => "customer",
            "billing_address" => array(
                "attention" => $attention_bill,
                "address" => $postData->billing->address_1,
                "street2" => $postData->billing->address_2,
                "city" => $postData->billing->city,
                "state" => $postData->billing->state,
                "zip" => $postData->billing->postcode,
                "country" => $postData->billing->country,
                "phone" => $postData->billing->phone
            ),
            "shipping_address" => array(
                "attention" => $attention_ship,
                "address" => $postData->shipping->address_1,
                "street2" => $postData->shipping->address_2,
                "city" => $postData->shipping->city,
                "state" => $postData->shipping->state,
                "zip" => $postData->shipping->postcode,
                "country" => $postData->shipping->country,
            ),
            "tax_id"=>$taxId,
            "is_taxable"=> ($taxId=="" ? false : $is_taxable),
            "tax_authority_id"=>($taxId=="" ? "" :$tax_authority_id),
            "tax_exemption_id"=>$tax_exemption_id,
        );

        $cntactData = $zohoBokObj->updateSalesOrderRequest($contactUpdateAry, 'contacts', BOOK_ORGID, true,$contactID);

        file_put_contents('contactlog.txt',"\n\n Updated Contact Id: ".print_r($contactID,TRUE), FILE_APPEND);
        file_put_contents('contactlog.txt',"\n JSONstring: ".json_encode($contactUpdateAry), FILE_APPEND);

        $contact_person_id =  $cntactData->contact->contact_persons[0]->contact_person_id;

        if(!empty($contact_person_id)){
            $contactPersonAry = array(
                "first_name" => ($postData->first_name == "")?$postData->billing->first_name:$postData->first_name,
                "last_name" => ($postData->last_name == "")?$postData->billing->last_name:$postData->last_name,
                "email" => $postData->email,
                "phone" => $postData->billing->phone
            ); 

            $contactDetail = $zohoBokObj->updateSalesOrderRequest2($contactPersonAry, 'contactpersons', BOOK_ORGID, true,$contact_person_id);

            file_put_contents('contactlog.txt',"\n\n contactDetail Update: ".print_r($contact_person_id,TRUE), FILE_APPEND);
        } 
    }   
}