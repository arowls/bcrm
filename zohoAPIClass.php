<?php

class zohoAPIClass {

    private $authtoken;
    private $ch;

    function __construct($authtoken) {
        $this->authtoken = $authtoken;
    }

    private function curlInit($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1); //standard i/o streams
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // Turn off the server and peer verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set to return data to string ($response)
        curl_setopt($ch, CURLOPT_POST, 1); //Regular post getRecordsfrominventoryusingID

        return $ch;
    }

    private function curlPostFields($query) {

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $query); // Set the request as a POST FIELD for curl.
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Authorization: Zoho-oauthtoken '.$this->authtoken));
        //Execute cUrl session
        $response = curl_exec($this->ch);

        return $response;
    }

    private function curlInitUpdate($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1); //standard i/o streams
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // Turn off the server and peer verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set to return data to string ($response)


        return $ch;
    }


    private function curlInitUpdateGet($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1); //standard i/o streams
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // Turn off the server and peer verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set to return data to string ($response)


        return $ch;
    }

    private function closeCurl() {
        curl_close($this->ch);
    }

    private function getCurlError() {
        echo 'Curl Error:<pre>';
        print_r(curl_error($this->ch));
        echo '<pre>';

        $this->closeCurl();
    }

    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function makeCURLRequest($postString, $moduleName,$orgId,$returnResponse = false) {
        $response = '';
        //manual curl call
        $url = 'https://crm.zoho.com/crm/private/xml/' . $moduleName . '/insertRecords';
        $xmlData = '<' . $moduleName . '> <row no="1">' . $postString . ' </row> </' . $moduleName . '>';

        $xmlData2 = urlencode($xmlData);

$query = $url."?newFormat=1&scope=crmapi&organization_id={$orgId}&xmlData={$xmlData2}";

        $this->ch = $this->curlInit($url);
        $results = $this->curlPostFields($query);

        $responseContacts = simplexml_load_string($results);

        if ($returnResponse) {
            $this->closeCurl();
            return $responseContacts;
        }
        $id = (string) $responseContacts->result->recorddetail->FL[0];

        if (!empty($id)) {
            $this->closeCurl();
            return $id;
        } else {
            return $query;
        }
    }

	/**
     * convet sales order to invoice
     */
    public function convertSalesOrderToInvoice($salesorderId,$returnResponse = false,$orgId) {
        //manual curl call
        $url = 'https://books.zoho.com/api/v3/invoices/fromsalesorder';

        $query = "organization_id={$orgId}&salesorder_id=".$salesorderId;

        $this->ch = $this->curlInit($url);

        $results = $this->curlPostFields($query);

        $response = json_decode($results);
       // print_r($response);
        $this->getCurlError();
    //    exit;
        if ($returnResponse) {
            return $response;
        }

        if ($response->code == 0) {
           $id = (string) $response->invoice->invoice_id;
        } else {
            return '';
        }
    }


    public function updateProductImg($imgPath, $moduleName, $orgId, $returnResponse = false, $itemID) {

        $ch = curl_init('https://books.zoho.com/api/v3/' . $moduleName . '/' . $itemID .'?organization_id='.$orgId);
        $photo = $this->makeCurlFile($imgPath);
        $data = array('image' => $photo);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Zoho-oauthtoken '.$this->authtoken));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
           $result = curl_error($ch);
        }
        curl_close ($ch);

        $response = json_decode($result);

        if ($response->code == 0) {
            $msg = (string) $response->message;
        }
        else{
            $msg = "There is error in uploading image";
        }
        return $msg;
    }

    public function makeCurlFile($file){
        $mime = mime_content_type($file);
        $info = pathinfo($file);
        $name = $info['basename'];
        $output = new CURLFile($file, $mime, $name);
        return $output;
    }
    /**
     * Search module
     */
    public function searchModule($moduleName, $criteria) {
        //manual curl call
        $url = 'https://crm.zoho.com/crm/private/xml/' . $moduleName . '/searchRecords';

        $query = $url."?scope=crmapi&criteria={$criteria}&newFormat=1";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Zoho-oauthtoken '.$this->authtoken));
        curl_setopt($ch, CURLOPT_URL, $query);
        $result = curl_exec($ch);
        curl_close($ch);

        $response = simplexml_load_string($result);

        $this->closeCurl();

        if (!empty($response)) {
            return $response;
        } else {
            return '';
        }
    }

    /**
     * Search module
     */
    public function convertLead($moduleName, $leaseId, $xmlData) {
        //manual curl call
        $url = 'https://crm.zoho.com/crm/private/xml/' . $moduleName . '/convertLead';

        $query = "scope=crmapi&leadId={$leaseId}&newFormat=1&xmlData={$xmlData}";

        $this->ch = $this->curlInit($url);
        $results = $this->curlPostFields($query);

        $response = simplexml_load_string($results);

        $this->closeCurl();

        if (!empty($response)) {
            return $response;
        } else {
            return '';
        }
    }

    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function makeBookRequest($postArray, $moduleName, $orgId, $returnResponse = false) {
        //manual curl call
        $url = 'https://books.zoho.com/api/v3/' . $moduleName;

        $jsonString = json_encode($postArray);

        $query = "organization_id={$orgId}&JSONString={$jsonString}";

        $this->ch = $this->curlInit($url);

        $results = $this->curlPostFields($query);

        $response = json_decode($results);
        $this->getCurlError();
        if ($returnResponse) {
            return $response;
        }

        if ($response->code == 0) {
            $id = '';
            if ($moduleName == 'contacts') {
                $id = (string) $response->contact->contact_id;
            } else if ($moduleName == 'invoices') {
                $id = (string) $response->invoice->invoice_id;
            } else if ($moduleName == 'salesorders') {
                $id = (string) $response->salesorder->salesorder_id;
            } else if ($moduleName == 'items') {
                $id = (string) $response->item->item_id;
            } else if ($moduleName == 'customerpayments') {
                $id = (string) $response->payment->payment_id;
            }
            return $id;
        } else {
            return '';
        }
    }


    public function makeBookRequesttest($postArray, $moduleName, $orgId, $returnResponse = false) {

        //manual curl call
        $url = 'https://books.zoho.com/api/v3/' . $moduleName;
        echo $url."<br>";
        $jsonString = json_encode($postArray);

        $query = "organization_id={$orgId}&JSONString={$jsonString}";
        echo $query."<br>";
        $this->ch = $this->curlInit($url);

        $results = $this->curlPostFields($query);

        $response = json_decode($results);
        $this->getCurlError();
        if ($returnResponse) {
            return $response;
        }

        print_r($results);

        if ($response->code == 0) {
            $id = '';
            if ($moduleName == 'contacts') {
                $id = (string) $response->contact->contact_id;
            } else if ($moduleName == 'invoices') {
                $id = (string) $response->invoice->invoice_id;
            } else if ($moduleName == 'salesorders') {
                $id = (string) $response->salesorder->salesorder_id;
            } else if ($moduleName == 'items') {
                $id = (string) $response->item->item_id;
            } else if ($moduleName == 'customerpayments') {
                $id = (string) $response->payment->payment_id;
            }
            return $id;
        } else {
            return '';
        }
    }
    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function updateSalesOrderRequest($postArray, $moduleName, $orgId, $returnResponse = false, $salesOrderID) {
        //manual curl call

        $url = 'https://books.zoho.com/api/v3/' . $moduleName . '/' . $salesOrderID;

        $jsonString = json_encode($postArray);
        $query = "organization_id={$orgId}&JSONString={$jsonString}";

        $this->ch = $this->curlInitUpdate($url);
        $results = $this->curlPostFields($query);

        $response = json_decode($results);

                //file_put_contents('contactlog.txt',"\n\n Contact Id: ".print_r($response), FILE_APPEND);


        if ($returnResponse) {
            return $response;
        }

        if ($response->code == 0) {
            $id = '';
            if ($moduleName == 'salesorders') {
                $id = (string) $response->salesorder->salesorder_id;
            }
            return $id;
        } else {
            return '';
        }
    }



     public function createContactRequest($postArray, $moduleName, $orgId) {
        //manual curl call

        $url = 'https://books.zoho.com/api/v3/'.$moduleName;

        $jsonString = json_encode($postArray);
        //$jsonString = "";
        $query = "organization_id={$orgId}&JSONString={$jsonString}";

        $this->ch = $this->curlInit($url);
        $results = $this->curlPostFields($query);

        $response = json_decode($results);

        if ($returnResponse) {
            return $response;
        }

        if ($response->code == 0) {
            $id = '';
            if ($moduleName == 'contacts') {
                $id = (string) $response->contact->contact_id;
            }
            return $id;
        } else {
            return '';
        }
    }


    public function updateSalesOrderRequest2($postArray, $moduleName, $orgId, $returnResponse = false, $salesOrderID) {
        //manual curl call

        $url = 'https://books.zoho.com/api/v3/contacts/' . $moduleName . '/' . $salesOrderID."?organization_id=".$orgId;

        $jsonString = json_encode($postArray);
        //$query = "authtoken={$this->authtoken}&organization_id={$orgId}&JSONString={$jsonString}";
        $query = "JSONString={$jsonString}";

        $this->ch = $this->curlInitUpdate($url);
        $results = $this->curlPostFields($query);

        $response = json_decode($results);

        if ($returnResponse) {
            return $response;
        }

        if ($response->code == 0) {
            $id = '';
            if ($moduleName == 'salesorders') {
                $id = (string) $response->salesorder->salesorder_id;
            }
            return $id;
        } else {
            return '';
        }
    }


    public function getRecordsfrominventoryusingID($authtoken,$moduleName, $orgId, $invoiceID) {


        $url = "https://inventory.zoho.com/api/v1/" . $moduleName . "/" . $invoiceID."?organization_id=".$orgId;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Zoho-oauthtoken '.$this->authtoken));
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);

        $items = json_decode($result, true);

        if ($items['code'] == 0) {
            $id = '';
            if ($moduleName == 'invoices') {
                $allrecords = $items['invoice'];
            }
            return $allrecords;
        } else {
            return '';
        }
    }

    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function updateInvoiceRequest($postArray, $moduleName, $orgId, $returnResponse = false, $invoiceID) {

        $url = 'https://inventory.zoho.com/api/v1/' . $moduleName . '/' . $invoiceID;

        $jsonString = json_encode($postArray);
        $query = "organization_id={$orgId}&JSONString={$jsonString}";

        $this->ch = $this->curlInitUpdate($url);
        $results = $this->curlPostFields($query);

        $response = json_decode($results);

        if ($returnResponse) {
            return $response;
        }

        if ($response->code == 0) {
            $id = '';
            if ($moduleName == 'invoices') {

                $id = (string) $response->invoice->invoice_id;
            }
            return $id;
        } else {
            return '';
        }
    }

    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function updatePaymentRequest($postArray, $moduleName, $orgId, $returnResponse = false, $paymentID) {

        $url = 'https://books.zoho.com/api/v3/' . $moduleName . '/' . $paymentID;

        $jsonString = json_encode($postArray);
        $query = "organization_id={$orgId}&JSONString={$jsonString}";

        $this->ch = $this->curlInitUpdate($url);
        $results = $this->curlPostFields($query);

        $response = json_decode($results);

        if ($returnResponse) {
            return $response;
        }

        if ($response->code == 0) {
            $id = '';
            if ($moduleName == 'customerpayments') {
                $id = (string) $response->payment->payment_id;
            }
            return $id;
        } else {
            return '';
        }
    }

    public function deletePaymentRequest($moduleName, $orgId, $returnResponse = false, $paymentID) {

        $api_request_url = 'https://books.zoho.com/api/v3/' . $moduleName . '/' . $paymentID . '?organization_id=' . $orgId;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json','Authorization: Zoho-oauthtoken '.$this->authtoken));
        curl_setopt($ch, CURLOPT_URL, $api_request_url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $api_response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($api_response, true);
        if ($result['code'] == '0') {
            if ($moduleName == 'customerpayments') {
                return $result['message'];
            }
        }
    }


    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function searchBookCRM($search, $moduleName, $orgId, $returnResponse = false) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/$moduleName?organization_id=$orgId&email=$search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE, // Turn off the server and peer verification
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Zoho-oauthtoken ".$this->authtoken),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return '';
        } else {
            $response = json_decode($response);
            // echo "<pre>";
            // print_r($response);
            if (empty($response->$moduleName)) {
                return '';
            }
            // die;
            foreach ($response->$moduleName as $key => $jsonObj) {
                # code...
                if ($jsonObj->email == urldecode($search)) {
                    $contact_id = (string) $jsonObj->contact_id;
                    return $contact_id;
                }
            }
            return '';
        }
    }



        /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function searchBookCRMByUserName($search, $moduleName, $orgId, $returnResponse = false) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/$moduleName?organization_id=$orgId&contact.contact_name=$search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE, // Turn off the server and peer verification
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Zoho-oauthtoken ".$this->authtoken),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        //print_r($response);

        if ($err) {
            return '';
        } else {
            $response = json_decode($response);

            if (empty($response->$moduleName)) {
                return '';
            }
            // die;
            foreach ($response->$moduleName as $key => $jsonObj) {
                # code...
                if ($jsonObj->contact_name == $search) {
                    $contact_id = (string) $jsonObj->contact_id;
                    return $jsonObj;
                }
            }
            return '';
        }
    }

    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function searchBookInvoice($search, $moduleName, $orgId, $returnResponse = false) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/$moduleName?organization_id=$orgId&orderno=$search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE, // Turn off the server and peer verification
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Zoho-oauthtoken ".$this->authtoken),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return '';
        } else {
            $response = json_decode($response);

            if (empty($response->$moduleName)) {
                return '';
            }
            // die;
            foreach ($response->$moduleName as $key => $jsonObj) {
                # code...
                //if ($jsonObj->orderno == $search) {
                $contact_id['invoice_id'] = (string) $jsonObj->invoice_id;
                $contact_id['customer_id'] = (string) $jsonObj->customer_id;
                return $contact_id;
                //}
            }
            return '';
        }
    }


    public function searchBookProductImg($itemId, $moduleName, $orgId, $returnResponse = false) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/$moduleName/$itemId/image?organization_id=$orgId",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE, // Turn off the server and peer verification
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Zoho-oauthtoken ".$this->authtoken),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return 'false';
        } else {
            $response = json_decode($response);

            if (empty($response->code)) {
                return 'true';
            }
            return 'false';
        }

    }
    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function searchBookCustomerPayment($search, $moduleName, $orgId, $returnResponse = false) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/$moduleName?organization_id=$orgId&reference_number=$search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE, // Turn off the server and peer verification
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Zoho-oauthtoken ".$this->authtoken),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return '';
        } else {
            $response = json_decode($response);

            if (empty($response->$moduleName)) {
                return '';
            }
            // die;
            foreach ($response->$moduleName as $key => $jsonObj) {
                # code...
                $contact_id['payment_id'] = (string) $jsonObj->payment_id;
                $contact_id['reference_number'] = (string) $jsonObj->reference_number;
                return $contact_id;
            }
            return '';
        }
    }

    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function searchBookSalesOrder($search, $moduleName, $orgId, $returnResponse = false) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/$moduleName?organization_id=$orgId&reference_number=$search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE, // Turn off the server and peer verification
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Zoho-oauthtoken ".$this->authtoken),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return '';
        } else {
            $response = json_decode($response);

            foreach ($response->$moduleName as $key => $jsonObj) {
                if ($jsonObj->reference_number == $search) {
                    $contact_id['salesorder_id'] = (string) $jsonObj->salesorder_id;
                    $contact_id['customer_id'] = (string) $jsonObj->customer_id;
                    $contact_id['reference_number'] = (string) $jsonObj->reference_number;
                    return $contact_id;
                }
            }
            return '';
        }
    }

    public function getRecordIDfromResponse($moduleName, $search) {

        if (empty($search->result)) {
            return '';
        }

        $recordId = '';

        $recordId = (string) $search->result->$moduleName->row->FL[0];

        if (empty($recordId)) {
            $recordId = (string) $search->result->$moduleName->row[0]->FL[0];
        }

        return $recordId;
    }

    public function APICall($url, $query) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Zoho-oauthtoken '.$this->authtoken));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query); // Set the request as a POST FIELD for curl
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

	 public function APICallGet($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Zoho-oauthtoken '.$this->authtoken));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function searchInvoiceByOrderId($orgId,$orderId) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/invoices?organization_id=$orgId&cf_order_id=$orderId",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE, // Turn off the server and peer verification
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Zoho-oauthtoken ".$this->authtoken),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return '';
        } else {
            $response = json_decode($response);

            if (empty($response->invoices)) {
                return '';
            }
            // die;

            return $response->invoices[0]->invoice_id;
        }
    }

    public function searchSalOrdByOrderId($orgId,$orderId) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/salesorders?organization_id=$orgId&cf_order_id=$orderId",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE, // Turn off the server and peer verification
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Zoho-oauthtoken ".$this->authtoken),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return '';
        } else {
            $response = json_decode($response);

            if (empty($response->salesorders)) {
                return '';
            }
            // die;

            return $response->salesorders[0]->salesorder_id;
        }
    }

    public function search_recordsByfield($zoho_category, $criteria, $fieldval, $orgId) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/".$zoho_category."?organization_id=".$orgId."&".$criteria."=".$fieldval,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "Authorization: Zoho-oauthtoken ".$this->authtoken
              ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          // echo "cURL Error #:" . $err;
            return '';
        } else {
          $res = json_decode($response);
          return $res->items[0]->item_id;
        }
    }

    public function search_recordsBysku($zoho_category, $criteria, $fieldval, $orgId) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/".$zoho_category."?organization_id=".$orgId."&".$criteria."=".$fieldval,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Zoho-oauthtoken ".$this->authtoken,
                "cache-control: no-cache"
              ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          // echo "cURL Error #:" . $err;
            return '';
        } else {
          $res = json_decode($response);
          if(count($res->items) == 0)
          	return 0;
          else
          	return $res->items[0]->item_id;
        }
    }

    public function search_recordsByskutest($zoho_category, $criteria, $fieldval, $orgId) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/".$zoho_category."?organization_id=".$orgId."&".$criteria."=".$fieldval,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Zoho-oauthtoken ".$this->authtoken,
                "cache-control: no-cache"
              ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          // echo "cURL Error #:" . $err;
            return '';
        } else {
          $res = json_decode($response);
          if(count($res->items) == 0)
            return 0;
          else
            return $res;
        }
    }

    public function search_recordsBytype($zoho_category, $criteria, $fieldval, $orgId) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/".$zoho_category."?organization_id=".$orgId."&".$criteria."=".$fieldval,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Zoho-oauthtoken ".$this->authtoken,
                "cache-control: no-cache"
              ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          // echo "cURL Error #:" . $err;
            return '00';
        } else {
            $res = json_decode($response);
            return $res;
        }
    }

    public function search_invoiceByitem($zoho_category, $criteria, $fieldval, $orgId) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://books.zoho.com/api/v3/".$zoho_category."?organization_id=".$orgId."&".$criteria."=".$fieldval,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Zoho-oauthtoken ".$this->authtoken,
                "cache-control: no-cache"
              ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          // echo "cURL Error #:" . $err;
            return '';
        } else {
          $res = json_decode($response);
            return $res;
        }
    }

    public function logContent($postdata,$reqtype,$comment){
        $date = new DateTime();
        $date = $date->format("m-d-Y h:i:s");
        if($reqtype == "request"){
            file_put_contents('log_file.txt', "\n========================\nOn ".$date."\n".$comment."\n".print_r($postdata, true) , FILE_APPEND);
        }
        else{
            file_put_contents('log_file.txt', "\nResponse:\n".$postdata , FILE_APPEND);
        }
    }
}

function logToFile($response, $filename = 'dns-log.txt', $addDateLine = false) {
    if ($filename == 'dns-log.txt') {
        $filename = 'dns-log-' . date("m-d-Y") . '.txt';
    } else {
        $filename = 'sales-ary-' . date("m-d-Y") . '.txt';
    }

    $filename = 'data/' . $filename;
    // open file
    $fd = fopen($filename, "a");
    // write string
    if ($addDateLine) {
        fwrite($fd, "================== " . date("d/m/Y h:i:s", mktime()) . " ====================" . "\n");
    }

    if (is_array($response)) {
        fwrite($fd, print_r($response, true));
    } else {
        fwrite($fd, $response . "\n");
    }
    // close file
    fclose($fd);
}

?>
