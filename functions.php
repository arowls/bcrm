<?php

	$zoho_apis_com = "https://books.zoho.com";
	$refresh_access_token_url = "https://accounts.zoho.com/oauth/v2/token";

	// Global vars to be used by below functions specific to this app
	// $zoho_client_id = "1000.IQ3X9WJZEL9X50707LJWLC6P2WQSJH";
	// $zoho_client_secret = "1680f5ad8c8cf3dbe8ef9fe93dec4dd6ce9995dbb6";
	// $zoho_redirect_uri = "https://boostedcrm.com/bigCommerce";  // will be URL to start.php in this example
	// $access_token_path = "books_access_token.dat";
	// $refresh_token_path = "books_refresh_token.dat";
	$access_token = "f78f9869e1ee93f59a267742cd3bfbb8";
	$organization_id = "685815954";

	$store_hash = "o23pa314c0";
	$store_authToken = "2d775uwgycokkdblbweborld0jqfojx";
	$store_authClient = "6bqwbu05e9loa92xmzm5b4hx1ae6ile";

	// function to use Zoho API v2
	function abZohoApi( $post_url, $post_fields, $post_header=false, $post_type='GET' )
	{
		// setup cURL request
		$ch=curl_init();

		// do not return header information
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// submit data in header if specified
		if(is_array($post_header)){
			curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
		}

		// do not return status info
		curl_setopt($ch, CURLOPT_VERBOSE, 0);

		// return data
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// cancel ssl checks
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		// if using GET, POST or PUT
		if($post_type=='POST'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
		} else if($post_type=='PUT'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_fields));
		} else if($post_type=='DELETE'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		}else{
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			if($post_fields){
				$post_url.='?'.http_build_query($post_fields);
				if(substr($post_url, -1) == "+"){
					$post_url = substr_replace($post_url, "", -1);
				}
			}
		}
// 		echo $post_url;

		// specified endpoint
		curl_setopt($ch, CURLOPT_URL, $post_url);
		// execute cURL request
		$response=curl_exec($ch);
		echo curl_getinfo($ch, CURLINFO_HTTP_CODE);
		// return errors if any
		if ($response === false) {
			$output = curl_error($ch);
		} else {
			$output = $response;
		}

		// close cURL handle
		curl_close($ch);

		// output
		return $output;
	}
	

	// get data: returns PHP Array (for functional sorting: PHP & JS)
	// usage: Leads: search_records("Leads", array('Last_Name:starts_with:G', 'Email:equals:someone@company.com'))
	function search_recordsById($zoho_category, $criteria){
		global $access_token, $zoho_apis_com, $organization_id;

		// endpoint
		$url_data=$zoho_apis_com."/api/v3/".$zoho_category."/".$criteria."?organization_id=".$organization_id;
		// echo $url_data;
		// echo " ".$access_token;
		// add access token to header
		$header_data=array("Authorization: ".$access_token, 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8');
		
		// send to Zoho API
		$response_json = abZohoApi($url_data, false, $header_data);
		/*echo "<pre>";
		print_r($response_json);*/
		// convert response to PHP array (for sorting)
		$response_arr = json_decode($response_json, true);

		// output
		return $response_arr;
	}

	function search_recordsByfield($zoho_category, $criteria, $fieldval){
		global $access_token, $zoho_apis_com, $organization_id;

		// endpoint
		$url_data=$zoho_apis_com."/api/v3/".$zoho_category;
		// echo $url_data;
		// echo " ".$access_token;
		// add access token to header
		$header_data=array("Authorization: ".$access_token, 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8');
		
		$user_fields = array("organization_id"=>$organization_id, $criteria=>$fieldval);
		// send to Zoho API
		$response_json = abZohoApi($url_data, $user_fields, $header_data);
		// echo "<pre>";
		// print_r($response_json);
		// convert response to PHP array (for sorting)
		$response_arr = json_decode($response_json, true);

		// output
		return $response_arr;
	}

	function create_record($zoho_category,$criteria=array()){
		global $access_token_path, $zoho_apis_com;

		// get access token
		$access_token = read_token($access_token_path);

		// endpoint
		$url_data=$zoho_apis_com."/crm/v2/".$zoho_category;

		$data_json = json_encode($criteria);
		// echo $data_json;
		// exit;
		// add access token to header
		$header_data=array("Authorization: Zoho-oauthtoken ".$access_token, 'Content-Type: application/json');

		// send to Zoho API
		// $response_json = abZohoApi($url_data, urlencode($xmlData), $header_data,'POST');
		$response_json = abZohoApi($url_data, $data_json, $header_data,'POST');

		// convert response to PHP array (for sorting)
		$response_arr = json_decode($response_json, true);
		// output
		return $response_arr;
	}


	function search_records($zoho_category, $criteria=array()){

		global $access_token_path, $zoho_apis_com;

		// get access token
		$access_token = read_token($access_token_path);
		// endpoint
		$url_data=$zoho_apis_com."/crm/v2/".$zoho_category."/search?criteria=";
		// echo $url_data;
		// join the criteria
		if(count($criteria)==1){
			$url_data.= '('.$criteria[0].')';
		}elseif(count($criteria)>1){
			$url_data.= '(('.implode($criteria, ') and (').'))';
		}
		// add access token to header
		$header_data=array("Authorization: Zoho-oauthtoken ".$access_token);

		// send to Zoho API
		$response_json = abZohoApi($url_data, false, $header_data);

		// convert response to PHP array (for sorting)
		$response_arr = json_decode($response_json, true);

		// output
		return $response_arr;
	}

	// function to ensure data was transferred
	// accepts array (JSON Response)
	// returns boolean
	function check_data_is_valid($data){
		$is_valid = false;
		if(isset($data['data'])){
			$is_valid = true;
		}
		return $is_valid;
	}

	function get_orderDetails($zoho_category,$criteria){
		global $store_hash, $store_authToken, $store_authClient;

		// $url_data="https://api.bigcommerce.com/stores/".$store_hash."/v2/orders/".$orderId;
		$url_data="https://api.bigcommerce.com/stores/".$store_hash."/v2/".$zoho_category."/".$criteria;

		// add access token to header
		$header_data=array("X-Auth-Token: ".$store_authToken, "X-Auth-Client: ".$store_authClient, 'Accept: application/json, Content-Type: application/json');
		
		// send to Zoho API
		$response_json = abZohoApi($url_data, false, $header_data, "GET");

		// convert response to PHP array (for sorting)
		$response_arr = json_decode($response_json, true);

		// output
		return $response_arr;	
	}

	function logContent($postdata,$reqtype,$comment){
		$date = new DateTime();
        $date = $date->format("m-d-Y h:i:s");
		if($reqtype == "request"){
			file_put_contents('log_file.txt', "\n========================\nOn ".$date."\n".$comment."\n".print_r($postdata, true) , FILE_APPEND);
		}
		else{
			file_put_contents('log_file.txt', "\nResponse:\n".$postdata , FILE_APPEND);
		}
	}

	function createRecord($postArray, $moduleName){
		global $access_token, $zoho_apis_com, $organization_id;

		// endpoint
		$url_data=$zoho_apis_com."/api/v3/".$moduleName;
		
		$jsonString = json_encode($postArray);
		//echo $jsonString;
		$query = "organization_id=".$organization_id."&JSONString={$jsonString}";
		
		$ch = curlInit($url_data);

		$header_data=array("Authorization: ".$access_token);
		
		$results = curlPostFields($query,$ch,$header_data);
		
		$response = json_decode($results);

		return $response;
	}
	
	function InvoiceSalesOrd($salesorder, $moduleName){
		global $access_token, $zoho_apis_com, $organization_id;

		// endpoint
		$url_data=$zoho_apis_com."/api/v3/".$moduleName;
		
		$query = "organization_id=".$organization_id."&salesorder_id=".$salesorder;
		
		$ch = curlInit($url_data);

		$header_data=array("Authorization: ".$access_token);
		
		$results = curlPostFields($query,$ch,$header_data);
		
		$response = json_decode($results);

		return $response;
	}

	function curlInit($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1); //standard i/o streams 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // Turn off the server and peer verification 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set to return data to string ($response) 
        

        return $ch;
    }

	function curlPostFields($query,$ch,$header) {
		curl_setopt($ch, CURLOPT_POST, 1); //Regular post getRecordsfrominventoryusingID
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); // Set the request as a POST FIELD for curl. 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query); // Set the request as a POST FIELD for curl. 
        //Execute cUrl session 
        $response = curl_exec($ch);

        return $response;
    }
    function curlGetFields($query,$ch,$header) {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        // curl_setopt($ch, CURLOPT_URL, $post_url);
        //Execute cUrl session 
        $response = curl_exec($ch);

        return $response;
    }