<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../wp-load.php');
require_once 'zohoAuthClass.php';
require 'zohoAPIClass.php';
require 'zohoInventoryAPIClass.php';

$zohoObj = new zohoClass();

$zohoObj->check_access_token();
$access_token_time_remaining = $zohoObj->get_time_remaining($zohoObj->access_token_path);

// determine minutes left
if($access_token_time_remaining<=5){
    $zohoObj->generate_access_token();
}

// if(isset($_GET['code'])){
//     // read get vars (code) generate refresh and access token.  Store refresh token in file.
//     $this_response_arr = $zohoObj->generate_refresh_token();

//     // get refresh token from file
//     $refresh_token = base64_decode( file_get_contents( $zohoObj->refresh_token_path ) );

//     // check refresh token exists and is of expected length
//     if(strlen($refresh_token)==70){
//         echo '<h1>Yay! All went well.</h1>';
//         echo '<p><b>Refresh</b> Token successfully generated and stored.</p><pre>';
//         // print_r($this_response_arr);
//         // echo '</pre>';
//     }else{
//         echo '<h2>Oops! Something went wrong.</h2>';
//         echo '<p><b>Refresh</b> token was not regenerated.</p><pre>';
//         print_r($this_response_arr);
//         echo '</pre>';
//     }
// }


$access_token = $zohoObj->read_token($zohoObj->access_token_path);

// define('AUTHTOKEN_BOOK', '80e6b535c6a96a172cb87b7ba04a0387');
// define('AUTHTOKEN_INV', '71e8760a5bebd1e12940315d053c1d66');
define('BOOK_ORGID', '681163660');

$json_row = file_get_contents('php://input');

$date = new DateTime();
$date = $date->format("m-d-Y h:i:s");
file_put_contents(date("Y-m-d-").'request.txt',"\n================> Product Updated<==================\nOn ".$date."\n".print_r($json_row,TRUE), FILE_APPEND);
$json = json_decode($json_row);
$postData = (object) $json;

class Zoho extends zohoAPIClass
{
}
$zohoBokObj   = new Zoho($access_token);

class ZohoInv extends zohoInventoryAPIClass
{
}
$zohoInvObj   = new ZohoInv($access_token);

$getItemsCategories = $zohoInvObj->searchInventoryCRM("categories",BOOK_ORGID);

if(count($postData) > 0){
    // Check whether item available for the given item sku
    $getItemsId = $zohoBokObj->search_recordsBysku("items","sku",$postData->sku,BOOK_ORGID);
    $getchartAcId = $zohoBokObj->search_recordsBytype("chartofaccounts","filter_by","AccountType.Active",BOOK_ORGID);
    // Find Breeder & Variety
    $productId = $postData->id;
    //Product And Category Meta fields
    $nm = preg_replace('/[^a-zA-Z0-9_ \#\/%\[\]\.\(\)%&-]/s', '', $postData->name);
     

   $breederArr = get_the_terms( $productId, 'breeder' );
    $varietyArr = get_the_terms( $productId, 'variety' );
    $breeder = "";
    if(count($breederArr) > 0){
        foreach ($breederArr as $key => $value) {
            $breeder .= $value->name;
            if(count($breederArr) > 1){
                $breeder .= ",";
            }
        }
        if( strpos($breeder,",") !== false ) {
             $breeder = substr($breeder, 0, -1);
        }
    }

    $variety = "";
    if(count($varietyArr) > 0){
        foreach ($varietyArr as $key => $value) {
            $variety .= $value->name;
            if(count($varietyArr) > 1){
                $variety .= ",";
            }
        }
        if( strpos($variety,",") !== false ) {
             $variety = substr($variety, 0, -1);
        }
    }

    // Find Auction Winner
    $auctionWinner = "";
    $auctionClosed = get_post_meta( $postData->id, '_auction_closed', true );

    if(!empty($auctionClosed) && $auctionClosed==2){
        $wining_user_id = get_post_meta( $postData->id, '_auction_current_bider', true );
        if($wining_user_id) {
            $user = get_user_by( 'ID', $wining_user_id );
            if(!empty($user->user_email)){
                $auctionWinner = $user->user_email;
            }
        }
    }

    $auctionBidCount = get_post_meta( $postData->id, '_auction_bid_count', true ); // Auction Bid Count
    $auctionOrderid ="";
    $auctionOrderid = get_post_meta( $postData->id, '_order_id', true ); // order id
    // Find Item Woo Status
    $itemWooStatus = "";
    $is_protected = false;

    $postStatus = get_post_status($postData->id);
    $post_data = get_post($postData->id);

    if($post_data->post_password != ''){
      $is_protected = true;
    }
    else {
      $is_protected = false;
    }

    if($postStatus == "publish"){
      $itemWooStatus = "Publish";
    }
    else if($postStatus == "future"){
      $itemWooStatus = "Scheduled";
    }
    
    else if($postStatus == "private"){
      $itemWooStatus = "Private";
    }
    else if($postStatus == "draft"){
      $itemWooStatus = "Draft";
    }
    else if($postStatus == "pending"){
      $itemWooStatus = "Pending Review";
    }

    if($is_protected == true){
      $itemWooStatus = "Password Protected";
    }

    //Woocomerce Product And Cateogry
    $metaData = wc_get_product($productId);
    $catArray = $tagArray = array();
    $catMeta = $tagMeta = "";
    
    if(count($metaData->category_ids)>0) {
        foreach ($metaData->category_ids as $key=>$value) {
            if($term = get_term_by( 'id', $value, 'product_cat' ) ){
                $catArray[] = $term->name;
            }
        }
        $catMeta = implode(",",$catArray);
    }

    if(count($metaData->category_ids)>0) {
        foreach ($metaData->tag_ids as $key=>$value) {
            if($term = get_term_by( 'id', $value, 'product_tag' ) ){
               $tagArray[] = $term->name;
            }
        }
        $tagMeta = implode(",",$tagArray);
    }
    //Woocomerce Product And Cateogry

    $itmUnit = "";
    $itmPurchaseDesc = "";
    $purchase_rate = "";
    $purchase_account = "";
    $sales_account = "";
    $inventory_account_id = "";
    $opening_stock = 1;
    $initial_stock_rate = 1;
    $item_type = "";
    $product_type = "";
   //Set Pieces to 1 by default and then check if we have a custom value  
    $pieces = 1;
    
    // Custom fields to add in items
    $arrCustom = array();
    // $arrCustom[] = array('label'=>"Short Description", 'value'=>strip_tags($postData->short_description));
    $sdescription = preg_replace('/[^a-zA-Z0-9_ \#\/%\[\]\.\(\)%&,-]/s', '', strip_tags($postData->short_description));

    $arrCustom[] = array('label'=>"Short Description", 'value'=> $sdescription);
    $arrCustom[] = array('label'=>"Breeder", 'value'=>$breeder);
    $arrCustom[] = array('label'=>"Variety", 'value'=>$variety);
    $arrCustom[] = array('label'=>"post_id", 'value'=>$productId);
    $syncDateStamp = new DateTime();
    $syncDateStamp = $syncDateStamp->format("Y-m-d");
    $arrCustom[] = array('label'=>"Last Sync Date", 'value'=>$syncDateStamp);


    foreach ($postData->meta_data as $key => $value) {
        if($value->key == "wc_custom_sex"){
            $valSex = "Unknown";
            if($value->value == "male")
                $valSex = "Male";
            else if($value->value == "female")
                $valSex = "Female";
            // $arrCustom[] = array('label'=>"Sex", 'value'=>$valSex);
            $arrCustom[] = array('label'=>"Sex (Gender)", 'value'=>$valSex);
        }
        if($value->key == "wc_custom_born_in"){
            $arrCustom[] = array('label'=>"Born In", 'value'=>$value->value);
        }
        if($value->key == "wc_custom_estimate_time"){
            $arrCustom[] = array('label'=>"Estimated Price", 'value'=>(int)str_replace("$","",$value->value));
        }
        if($value->key == "_auction_dates_from"){
            $arrCustom[] = array('label'=>"Auction Start", 'value'=>$value->value);
        }
        if($value->key == "_auction_dates_to"){
            $arrCustom[] = array('label'=>"Auction End", 'value'=>$value->value);
        }
       if($value->key == "wc_custom_size"){
            $arrCustom[] = array('label'=>"Inches", 'value'=>$value->value);
        }
        if($value->key == "pond_id"){
            $arrCustom[] = array('label'=>"Pond ID", 'value'=>$value->value);
        }
        if($value->key == "boarding"){
            $arrCustom[] = array('label'=>"Boarding", 'value'=>$value->value);
        }
        if($value->key == "boarding_until"){
            $arrCustom[] = array('label'=>"Boarding until", 'value'=>$value->value);
        }
        if($value->key == "_auction_start_price"){
            $arrCustom[] = array('label'=>"Starting Price", 'value'=>$value->value);
        }
        
         if($value->key == "sales_account"){
            foreach($getchartAcId->chartofaccounts as $indx => $acval){
                if($acval->account_name == $value->value){
                    $sales_account = $acval->account_id;
                }
            }
        }

        if($value->key == "purchase_account"){
            foreach($getchartAcId->chartofaccounts as $indx => $acval){
                if($acval->account_name == $value->value){
                    $purchase_account = $acval->account_id;
                }
            }
        }

        if($value->key == "inventory_account"){
            foreach($getchartAcId->chartofaccounts as $indx => $acval){
                if($acval->account_name == $value->value){
                    $inventory_account_id = $acval->account_id;
                }
            }
        }

        if($value->key == "unit"){
            $itmUnit = $value->value;
        }

        // if($value->key == "status"){
        //     $arrCustom[] = array('label'=>"status", 'value'=>$value->value);
        // }

        if($value->key == "purchase_price"){
            $price =  trim(str_replace("USD","",$value->value));
            $purchase_rate = $price;
        }

        if($value->key == "purchase_description"){
            $itmPurchaseDesc = $value->value;
        }

        if($value->key == "opening_stock"){
            $opening_stock = $value->value;
        }

        if($value->key == "opening_stock_value"){
            $price =  trim(str_replace("USD","",$value->value));
            $initial_stock_rate = $price;
        }

        if($value->key == "item_type"){
            $item_type = strtolower($value->value);
        }

        if($value->key == "product_type"){
            $product_type = $value->value;
        }

        if($value->key == "cm_size"){
            $arrCustom[] = array('label'=>"CM - Size", 'value'=>$value->value);
        }

        if($value->key == "category_name"){
            $category_id = $getItemsCategories[$value->value];
            $category_name = $value->value;         
        }
        if($value->key == "cf_pieces"){
          $pieces = $value->value;
         }
      
    }//End foreach
    
	$auctionRelistDate = get_post_meta( $postData->id, '_auction_relisted', true ); // Auction Relist Date

    if(!empty($auctionClosed) && $auctionClosed==2 && $postData->in_stock==true && $auctionBidCount > 0 && $auctionOrderid == ""){
        $arrCustom[] = array('label'=>"Auction Status", 'value'=>'Unpaid Winner');          
    }
    else if(empty($auctionClosed) && !empty($auctionRelistDate)){
        $arrCustom[] = array('label'=>"Auction Status", 'value'=>'Relisted');
        $arrCustom[] = array('label'=>"Auction Highest Bidder", 'value'=>'');
    } 
    else if(!empty($auctionClosed) && $auctionClosed==1 && $postData->in_stock==true && $auctionBidCount == 0 && $auctionOrderid == ""){
        $arrCustom[] = array('label'=>"Auction Status", 'value'=>'No Bid Koi');
        $arrCustom[] = array('label'=>"Auction Highest Bidder", 'value'=>'');

    }
     else if(!empty($auctionClosed) && $auctionClosed==2 && $postData->in_stock==false && $auctionBidCount > 0 && $auctionOrderid == ""){
            $arrCustom[] = array('label'=>"Auction Status", 'value'=>'Paid');
        }
    else if(!empty($auctionClosed) && $auctionClosed==2 && $auctionBidCount > 0 && $auctionOrderid != ""){
        $arrCustom[] = array('label'=>"Auction Status", 'value'=>'Paid Online');
    }
  
        
    if($pieces > 0 ){
        $arrCustom[] = array('label'=>"Pieces", 'value'=>$pieces);
    }
        
    if($auctionBidCount > 0 ) {
        $arrCustom[] = array('label'=>"Auction Bid Count", 'value'=>$auctionBidCount);
    }

    if($auctionWinner != ""){
        $arrCustom[] = array('label'=>"Auction Highest Bidder", 'value'=>$auctionWinner);
    }

    if($itemWooStatus != ""){
        $arrCustom[] = array('label'=>"Item Woo Status", 'value'=>$itemWooStatus);
    }

    //Product And Category Meta fields
    if($tagMeta!=""){
        $arrCustom[] = array('label'=>"Woo Product Tag", 'value'=>$tagMeta);
    }

    if($catMeta!=""){
        $catMeta = preg_replace('/[^a-zA-Z0-9_ \#\/%\[\]\.\(\)%&\-,\n]/s', '', $catMeta);
        $arrCustom[] = array('label'=>"Woo Product Category", 'value'=>$catMeta);

    }
    //Custom Thumbnail for Picklist / Reporting
	if($postData->images[0]->src != ""){
	  $arrCustom[] = array('label'=>"Woo Product Thumb", 'value'=>$postData->images[0]->src);
	}
      //Clear Auction Data if product category is not RA	   
	if (strpos($category_name, "RA") == null) {
			file_put_contents(date("Y-m-d-").'request.txt','before unset :'.print_r($arrCustom), FILE_APPEND);

            unset($arrCustom["Auction Status"]);
	        $arrCustom[] = array('label'=>"Auction Status", 'value'=>'');
            unset($arrCustom["Auction Highest Bidder"]);
			$arrCustom[] = array('label'=>"Auction Highest Bidder", 'value'=>'');
			file_put_contents(date("Y-m-d-").'request.txt','\nAFTER unset :'.print_r($arrCustom), FILE_APPEND);

            }

   $salesItemAry = array(
        "name" => $nm,
        "sku" => $postData->sku,
        "rate" => ($postData->price=="" ? 0 : $postData->price), 
        "category_id" =>($category_id=="" ? '1655091000001340151' : $category_id ),
        "category_name" => (isset($category_name) ? $category_name : 'KOI'),
        "unit" =>($itmUnit=="" ? "pcs" : $itmUnit),
        "description" =>  $nm,
        "initial_stock" => $opening_stock,
        "initial_stock_rate" => ($initial_stock_rate=="" ? 1 : $initial_stock_rate),
        "item_type" => "inventory",
        "product_type" => "goods",
        "account_id" => ($sales_account==""  ? '1655091000000000388' :  $sales_account),
        "purchase_account_id" => ($purchase_account=="" ? '1655091000000034003' : $purchase_account),
        "inventory_account_id" => ($inventory_account_id=="" ? '1655091000000034001' : $inventory_account_id ),
        "purchase_rate" => $purchase_rate,
        "purchase_description" => $itmPurchaseDesc,
        "custom_fields" => $arrCustom
    );

    if(empty($getItemsId) || $getItemsId=="" || $getItemsId==0){
        
        // Create item with the given request fields
        $saleItemID = $zohoBokObj->makeBookRequest($salesItemAry, 'items', BOOK_ORGID,true);
        $getItemsId = $saleItemID->item->item_id;
        file_put_contents(date("Y-m-d-").'request.txt',"\nNew Product Id: ".$getItemsId, FILE_APPEND);
    file_put_contents(date("Y-m-d-").'request.txt',"\n JSONstring: ".json_encode($salesItemAry), FILE_APPEND);

    }
    else{
		 
        //Remove default values on update from Woo in case they have been set in Zoho 
          if ($category_id=="") {
            unset($salesItemAry["category_id"]);
            unset($salesItemAry["category_name"]);
          }
            if ($purchase_rate==""){
            unset($salesItemAry["purchase_rate"]);
            unset($salesItemAry["purchase_description"]);
        }
    
            
        file_put_contents('request.txt',"\n Updated Product Id: ".$getItemsId , FILE_APPEND);
        // Update item with the given request fields
        $updtItem = $zohoBokObj->updateSalesOrderRequest($salesItemAry, 'items', BOOK_ORGID, true,$getItemsId);
        file_put_contents(date("Y-m-d-").'request.txt',"\n update item: ".print_r($updtItem), FILE_APPEND);
        file_put_contents(date("Y-m-d-").'request.txt',"\n JSONstring: ".json_encode($salesItemAry), FILE_APPEND);

    }

    // Create image of item in zoho books
    if($postData->images[0]->src != ""){
        $isImg = $zohoBokObj->searchBookProductImg($getItemsId,"items",BOOK_ORGID,true);
        if($isImg == "false"){
            // file_put_contents("images/".$postData->images[0]->name.".jpg", fopen($postData->images[0]->src, 'r'));
            // $imgPath = "images/".$postData->images[0]->name.".jpg";
            $imgPath = str_replace("https://www.kodamakoifarm.com", "..", $postData->images[0]->src);
            $itemUpdt = $zohoBokObj->updateProductImg($imgPath, 'items', BOOK_ORGID, true,$getItemsId."/image");
            file_put_contents(date("Y-m-d-").'request.txt',"\n Image Upload Message: ".$itemUpdt, FILE_APPEND);
        }
   }
}