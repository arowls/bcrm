<?php

class zohoInventoryAPIClass {

    private $authtoken;
    private $ch;

    function __construct($authtoken) {
        $this->authtoken = $authtoken;
    }

    private function curlInit($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1); //standard i/o streams
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // Turn off the server and peer verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set to return data to string ($response)
        curl_setopt($ch, CURLOPT_POST, 1); //Regular post getRecordsfrominventoryusingID

        return $ch;
    }

    private function curlPostFields($query) {

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $query); // Set the request as a POST FIELD for curl.
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Authorization: Zoho-oauthtoken '.$this->authtoken));
        //Execute cUrl session
        $response = curl_exec($this->ch);

        return $response;
    }

    private function curlInitUpdate($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1); //standard i/o streams
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // Turn off the server and peer verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set to return data to string ($response)


        return $ch;
    }


    private function curlInitUpdateGet($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1); //standard i/o streams
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // Turn off the server and peer verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set to return data to string ($response)


        return $ch;
    }

    private function closeCurl() {
        curl_close($this->ch);
    }

    private function getCurlError() {
        echo 'Curl Error:<pre>';
        print_r(curl_error($this->ch));
        echo '<pre>';

        $this->closeCurl();
    }

    /**
     * $postString zoho post string
     * $moduleName zoho module name
     */
    public function searchInventoryCRM($moduleName, $orgId, $returnResponse = false) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://inventory.zoho.com/api/v1/$moduleName?organization_id=$orgId",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => FALSE, // Turn off the server and peer verification
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Zoho-oauthtoken ".$this->authtoken),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return '';
        } else {
            $response = json_decode($response);

            $arrCat = array();
            foreach($response->categories as $key => $catattr){
                $arrCat[$catattr->name] = $catattr->category_id;
            }
            return $arrCat;
        }
    }
}

?>
