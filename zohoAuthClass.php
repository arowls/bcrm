<?php

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

class zohoClass {

    private $authtoken;
    private $ch;

   	// Global vars for Zoho API
	private $zoho_apis_com = "https://www.zohoapis.com";
	private $zoho_apis_eu = "https://www.zohoapis.eu";
	private $refresh_access_token_url = "https://accounts.zoho.com/oauth/v2/token";

	// Endpoint: Sandbox  // disable after testing
	private $zoho_sandbox = "https://sandbox.zohoapis.eu";
	private $zoho_sandbox_domain = "https://crmsandbox.zoho.eu/crm";

	// Global vars to be used by below functions specific to this app
	private $zoho_client_id = "1000.4P2U0KCSVGT9JCFGH056A7WVUIENBC";
	private $zoho_client_secret = "b221051b206b4ca8f62c64de84f08651d5a8882eea";
	private $zoho_redirect_uri = "https://www.kodamakoifarm.com/bcrm/books_oauth_token.php";  // will be URL to start.php in this example
	public $access_token_path = "books_access_token.dat";
    public $refresh_token_path = "books_refresh_token.dat";    

    function __construct() {
        //$this->authtoken = AUTHTOKEN;
    }

    //Token Generate--------------------------------------------------------------------
    public function abZohoApi( $post_url, $post_fields, $post_header=false, $post_type='GET' )
	{
		// setup cURL request
		$ch=curl_init();

		// do not return header information
		curl_setopt($ch, CURLOPT_HEADER, 0);

		// submit data in header if specified
		if(is_array($post_header)){
			curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
		}

		// do not return status info
		curl_setopt($ch, CURLOPT_VERBOSE, 0);

		// return data
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// cancel ssl checks
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		// if using GET, POST or PUT
		if($post_type=='POST'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
		} else if($post_type=='PUT'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_fields));
		} else if($post_type=='DELETE'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		}else{
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			if($post_fields){
				$post_url.='?'.http_build_query($post_fields);
			}
		}

		// specified endpoint
		curl_setopt($ch, CURLOPT_URL, $post_url);

		// execute cURL request
		$response=curl_exec($ch);

		// return errors if any
		if ($response === false) {
			$output = curl_error($ch);
		} else {
			$output = $response;
		}

		// close cURL handle
		curl_close($ch);

		// output
		return $output;
    }


    public function generate_refresh_token(){

        // Generate Access Token and Refresh Token - Read url GET values
        $zoho_grant_token = $_GET['code'];
        $zoho_location = $_GET['location'];
        $zoho_accounts_server = $_GET['accounts-server'];

        // Generate Access Token and Refresh Token
        $url_auth=urldecode($zoho_accounts_server)."/oauth/v2/token";

        // Build fields to post
        $fields_token=array("code"=>$zoho_grant_token, "redirect_uri"=> $this->zoho_redirect_uri, "client_id"=> $this->zoho_client_id, "client_secret"=> $this->zoho_client_secret, "grant_type"=>"authorization_code", "prompt"=>"consent");

        // Generate Access Token and Refresh Token - post via cURL
        $response_json = $this->abZohoApi($url_auth, $fields_token, false, 'POST');

        // Generate Access Token and Refresh Token - format output (convert JSON to Object)
        $refresh_token_arr = json_decode($response_json, true);

        // store in var
        $refresh_token = isset($refresh_token_arr['refresh_token']) ? $refresh_token_arr['refresh_token'] : 0;

        // encode value to base64
        $refresh_token_base64 = base64_encode($refresh_token);

        // store encoded value to file
        file_put_contents($this->refresh_token_path, $refresh_token_base64);

        // -- do access token while we're here
        // store in access token
        $access_token = isset($refresh_token_arr['access_token']) ? $refresh_token_arr['access_token'] : 0;

        // encode value to base64
        $access_token_base64 = base64_encode($access_token);

        // store encoded value to file
        file_put_contents($this->access_token_path, $access_token_base64);

        // return array of json objects
        return $refresh_token_arr;
    }


    // function to generate access token from refresh token
	// returns minutes remaining of valid token
	public function generate_access_token(){

		// get refresh token from file
		$refresh_token = base64_decode( file_get_contents( $this->refresh_token_path ) );

		// build fields to post
		$refresh_fields = array("refresh_token" => $refresh_token, "client_id" => $this->zoho_client_id, "client_secret" => $this->zoho_client_secret, "grant_type" => "refresh_token");

		// send to Zoho API
		$this_access_token_json = $this->abZohoApi($this->refresh_access_token_url, $refresh_fields, false, 'POST');

		// convert JSON response to array
		$access_token_arr = json_decode($this_access_token_json, true);

		// store in var
		$returned_token = $access_token_arr['access_token'];
		
		// encode value to base64
		$access_token_base64 = base64_encode($returned_token);

		// store encoded value to file
		file_put_contents($this->access_token_path, $access_token_base64);
    }

    // function to get minutes left on a generated file
	// defaults to an hour expiry time
	// usage: get_time_remaining( 'access.dat', 3600)
	public function get_time_remaining($file, $expiry_in_seconds=3600){

		// get file modified time
		$file_modified_time = filemtime($file);

		// add 1 hour
		$file_expiry_time = $file_modified_time + $expiry_in_seconds;

		// calculate seconds left
		$diff = $file_expiry_time - time();

		// round to minutes
		$minutes = floor($diff/60);

		// output
		return $minutes;
	}


	// function to check access token and regenerate if necessary
	public function check_access_token(){

		// get time remaining on access token (1 hour max)
		$access_token_time_remaining = $this->get_time_remaining($this->access_token_path);
		
		// if less than 5 minutes left, regenerate token
		if($access_token_time_remaining<=5){

			// Generate Access Token from Refresh Token
			$this->generate_access_token();

			// update time remaining on access token (again)
			$access_token_time_remaining = $this->get_time_remaining($this->access_token_path);
		}

		// return time remaining (in minutes)
		return $access_token_time_remaining;

	}
    

    // function to decode and read access token from file
	function read_token($file){

		// get access token from file
		$token_base64 = file_get_contents($file);

		// decode value to token
		$token = base64_decode($token_base64);

		// output
		return $token;
	}    


    //Token Generatep--------------------------------------------------------------------

    private function curlInit($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1); //standard i/o streams 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // Turn off the server and peer verification 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set to return data to string ($response) 
        curl_setopt($ch, CURLOPT_POST, 1); //Regular post 

        return $ch;
    }

    private function curlPostFields($query) {

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $query); // Set the request as a POST FIELD for curl. 
        //Execute cUrl session 
        $response = curl_exec($this->ch);

        return $response;
    }

    private function closeCurl() {
        curl_close($this->ch);
    }

    public function getCurlError() {
        echo 'Curl Error:<pre>';
        print_r(curl_error($this->ch));
        echo '<pre>';

        $this->closeCurl();
    }


    public function searchZohoRecordByIDV2($moduleName, $field, $value) {

        $url = "https://www.zohoapis.com/crm/v2/".$moduleName."/search?criteria=(".$field.":equals:".$value.")";               

        //https://www.zohoapis.com/crm/v2/Leads/search?criteria=((Last_Name:equals:Burns\,B)and(First_Name:starts_with:M))

        $access_token = $this->read_token($this->access_token_path);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Zoho-oauthtoken '.$access_token));
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }

    public function search_recordsBysku($zoho_category, $criteria, $fieldval, $orgId) {

    	$url = "https://books.zoho.com/api/v3/".$zoho_category."?organization_id=".$orgId."&".$criteria."=".$fieldval;

        $access_token = $this->read_token($this->access_token_path);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Zoho-oauthtoken '.$access_token));
        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
          // echo "cURL Error #:" . $err;
            return '';
        } else {
          $res = json_decode($response);
          return $res;
          // if(count($res->items) == 0)
          //   return 0;
          // else
          //   return $res->items[0]->item_id;
        }
    }
}
?>