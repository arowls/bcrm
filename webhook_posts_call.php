<?php
error_reporting(-1);
ini_set('display_errors', true);
require_once('../wp-load.php');
require_once 'zohoAuthClass.php';
require 'zohoPostAPIClass.php';

$zohoObj = new zohoClass();

$zohoObj->check_access_token();
$access_token_time_remaining = $zohoObj->get_time_remaining($zohoObj->access_token_path);

// determine minutes left
if($access_token_time_remaining<=5){
    $zohoObj->generate_access_token();
}

// if(isset($_GET['code'])){
//     // read get vars (code) generate refresh and access token.  Store refresh token in file.
//     $this_response_arr = $zohoObj->generate_refresh_token();

//     // get refresh token from file
//     $refresh_token = base64_decode( file_get_contents( $zohoObj->refresh_token_path ) );

//     // check refresh token exists and is of expected length
//     if(strlen($refresh_token)==70){
//         echo '<h1>Yay! All went well.</h1>';
//         echo '<p><b>Refresh</b> Token successfully generated and stored.</p><pre>';
//         // print_r($this_response_arr);
//         // echo '</pre>';
//     }else{
//         echo '<h2>Oops! Something went wrong.</h2>';
//         echo '<p><b>Refresh</b> token was not regenerated.</p><pre>';
//         print_r($this_response_arr);
//         echo '</pre>';
//     }
// }

$access_token = $zohoObj->read_token($zohoObj->access_token_path);

// define('AUTHTOKEN_BOOK', '80e6b535c6a96a172cb87b7ba04a0387');
define('BOOK_ORGID', '681163660');

file_put_contents(date("Y-m-d-").'log_file.txt','================> New ORDER Started <==================', FILE_APPEND);

$json_row = file_get_contents('php://input');

$json = json_decode($json_row);
$postData = (object) $json;

$date = new DateTime();
$date = $date->format("m-d-Y h:i:s");

file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n========================\nOn ".$date."\n".print_r($json_row,TRUE), FILE_APPEND);
class Zoho extends zohoPostAPIClass
{
}

$adjustmentArr=array();
// To Calculate item fees tax, total fees, total tax
foreach( $postData->fee_lines as $item_id => $item_fee ){
file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n========================\n item_fee->name ".$date."\n".$item_fee->name, FILE_APPEND);

    if($item_fee->total>0 || $item_fee->total_tax>0){
        $feeAmount=$item_fee->total+$item_fee->total_tax;
		if($item_fee->name=="Breeder Certificates"){
			 $bCertsAry =array();
		  $bCerts=array(
                "name" => urlencode ($item_fee->name),
                "item_id" => "1655091000002731920",
                "item_order" => $line_item->quantity,
                "rate" => "15.00",
                "quantity" => $item_fee->total/15,
                "item_total" => $item_fee->total
            );
			$bCertsAry[] = $bCerts;
file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n========================\n bCerts ".$date."\n".print_r($bCerts,TRUE), FILE_APPEND);			
			
		}
		else {
        $adjustmentArr['adjustment_description']=urlencode($item_fee->name);
        $adjustmentArr['adjustment']=$feeAmount;
		}
    }else if($item_fee->total<0 || $item_fee->total_tax<0){
        $feeAmount=abs(round($item_fee->total, 2))+abs(round($item_fee->total_tax,2));
        $adjustmentArr['adjustment_description']=urlencode($item_fee->name);
        $adjustmentArr['adjustment']=-$feeAmount;
    }
}

$discount_per_item = 0;
// To caculate item discount, discount tax
foreach( $postData->coupon_lines as $item_id => $item_fee ){
    if($item_fee->discount>0 || $item_fee->discount_tax>0){
        $discount_per_item=$item_fee->discount+$item_fee->discount_tax;
    }else if($item_fee->discount<0 || $item_fee->discount_tax<0){
        $discount_per_item=abs(round($item_fee->discount, 2))+abs(round($item_fee->discount_tax,2));
    }
}

$zohoBokObj   = new Zoho($access_token);
$flag_insert  = true;

if($postData->id>0)
{
    // To check weather sales order and invoice exist
    $invoiceId = $zohoBokObj->searchInvoiceByOrderId(BOOK_ORGID,$postData->id);
    $salesordId = $zohoBokObj->searchSalOrdByOrderId(BOOK_ORGID,$postData->id);
    if(!empty($invoiceId) and $invoiceId!="" and $invoiceId>0){
        $flag_insert = false;
        file_put_contents(date("Y-m-d-").'log_file.txt','==> FOUND ID IN INVOICE <==', FILE_APPEND);
        file_put_contents(date("Y-m-d-").'log_file.txt',print_r($postData,true), FILE_APPEND);
    }
}

$postData->billing  = (object) $postData->billing;
$postData->shipping = (object) $postData->shipping;

// To check contact exist for email address
$email=strtolower($postData->billing->email);
if($email !=""){
    $contactID = $zohoBokObj->searchBookCRM($email,'contacts',BOOK_ORGID);
    file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\nEmail Searched: ".$email , FILE_APPEND);

    file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n Found Contact Id: ".json_encode($contactID), FILE_APPEND);
}else{
    $contactID="";
}

// To find tax id from perticular Customer role, country, state and postcode
    $taxId="";
	$tax_exemption_id ="";
	$current_user_data = get_userdata($postData->customer_id);
    file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\ncustomer data : ".print_r($current_user_data->roles), FILE_APPEND);

	$is_taxable = true;

if($postData->shipping->country=="CA"){
        $taxId="1655091000000250005";
        $tax_authority_id ="1655091000000252003";
	}else if($postData->shipping->country=="US" && $postData->shipping->state=="HI" && ($postData->tax_lines[0]->rate_percent == "0.5" )){
		$taxId="1655091000000082015";
        $tax_authority_id ="1655091000000070025";
	}else if (count(array_intersect($current_user_data->roles,  array('wholesale'))) > 0) {
		$taxId="1655091000000288019";
        $tax_authority_id ="1655091000000252003";
		$tax_exemption_id = "1655091000000079023";
		$is_taxable = false;
	}else if($postData->shipping->country=="US" && $postData->shipping->state=="FL"){
		$taxId="1655091000000085001";
		$tax_authority_id ="1655091000000082021";
	}else if($postData->shipping->country=="US" && $postData->shipping->state=="HI" && ($postData->tax_lines->rate_percent == "4.1660" )){
		$taxId="1655091000007884435";
		$tax_authority_id="1655091000000070025";    
	}else if($postData->shipping->country=="US" && $postData->shipping->state=="HI"){
		$taxId="1655091000007884425";
		$tax_authority_id="1655091000000070025";

	}else if($postData->shipping->country=="US" && $postData->shipping->state=="NY"){
        $taxId="1655091000000250023";
        $tax_authority_id ="1655091000000252003";
		}
else{
		$taxId="1655091000000288019";
		$tax_authority_id ="1655091000000252003";
		//$tax_exemption_id = "1655091000000252007";
		//$is_taxable = false;
}

$paymentStatus = str_replace("-"," ",$postData->status);
$paymentStatus = ucwords($paymentStatus);

//If no Invoice was found we continue with creationif
if($flag_insert)
{
    $response = '';
    //If not Contact found, we Create one
    //strip breaking characters from  notes variable 
     $cleanNotes = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $postData->customer_note);

    if (empty($contactID))    {
        $contName = '';
        if (empty($postData->billing->company)) {
            $contName = $postData->billing->first_name . ' ' . $postData->billing->last_name;
        } else {
            $contName = $postData->billing->company;
        }


        // Array to create contact in zoho books
        $contactAry = array(
            "contact_name" => $contName,
            "company_name" => $postData->billing->company,
            "contact_type" => "customer",
            "billing_address" => array(
                "address" => $postData->billing->address_1 . ' ' . $postData->billing->address_2,
                "city" => $postData->billing->city,
                "state" => $postData->billing->state,
                "zip" => $postData->billing->postcode,
                "country" => $postData->billing->country
            ),
            "shipping_address" => array(
                "address" => $postData->shipping->address_1 . ' ' . $postData->shipping->address_2,
                "city" => $postData->shipping->city,
                "state" => $postData->shipping->state,
                "zip" => $postData->shipping->postcode,
                "country" => $postData->shipping->country
            ),
            "contact_persons" => array(array(
                    "first_name" => $postData->billing->first_name,
                    "last_name" => $postData->billing->last_name,
                    "email" => $postData->billing->email,
                    "phone" => $postData->billing->phone,
                    "is_primary_contact" => true
            )),
            "tax_id"=>$taxId,
            "is_taxable"=> ($taxId=="" ? false : $is_taxable),
            "tax_authority_id"=>($taxId=="" ? "" : $tax_authority_id),
            "tax_exemption_id"=>$tax_exemption_id,
            "notes" => urlencode($cleanNotes),
            "email" => $postData->billing->email,
            "is_primary_contact" => true,
        );
        // Create new contact in zoho books
	    file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n ContactAry: ".json_encode($contactAry), FILE_APPEND);
        $contactID = $zohoBokObj->makeBookRequest($contactAry, 'contacts', BOOK_ORGID,true);
        $contactID = $contactData->contact->contact_id;
        file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n New Contact Id: ".$contactID, FILE_APPEND);
		
    }
    else{
         $contName = '';
        if (empty($postData->billing->company)) {
            $contName = $postData->billing->first_name . ' ' . $postData->billing->last_name;
        } else {
            $contName = $postData->billing->company;
        }

        // Array to update contact in zoho books
        $contactAry = array(
            "contact_name" => urlencode($contName),
            "company_name" => urlencode($postData->billing->company),
            "billing_address" => array(
                "address" => urlencode($postData->billing->address_1 . ' ' . $postData->billing->address_2),
                "city" => urlencode($postData->billing->city),
                "state" => urlencode($postData->billing->state),
                "zip" => urlencode($postData->billing->postcode),
                "country" => urlencode($postData->billing->country)
            ),
            "shipping_address" => array(
                "address" => urlencode($postData->shipping->address_1 . ' ' . $postData->shipping->address_2),
                "city" => urlencode($postData->shipping->city),
                "state" => urlencode($postData->shipping->state),
                "zip" => urlencode($postData->shipping->postcode),
                "country" => urlencode($postData->shipping->country)
            ),
            "tax_id"=>$taxId,
            "is_taxable"=> ($taxId=="" ? false : $is_taxable),
            "tax_authority_id"=>($taxId=="" ? "":$tax_authority_id ),
            "tax_exemption_id"=>$tax_exemption_id,
            "notes" => urlencode($cleanNotes),
            "is_primary_contact" => true,
        );
        // update contact by id in zoho books
        $cntctID222 = $zohoBokObj->updateSalesOrderRequest($contactAry, 'contacts', BOOK_ORGID, true,$contactID);
		file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n Updated Contact Id: ".json_encode($contactID), FILE_APPEND);
        file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n Updated Info: ".$contactAry, FILE_APPEND);
    }

    $payment = false;

    if($postData->status == 'completed'){
        $payment = true;
    }

    if (!empty($contactID))
    {
        $lineItems = array();
        $nm = '';
        $ok = true;
        $itemID = array();
        $wholesale = false;
        foreach ($postData->line_items as $key => $line_item)
        {
            $line_item = (object)$line_item;
            $nm = $line_item->name;
            $nm = preg_replace('/[^a-zA-Z0-9_ \/%\[\]\.\(\)%&-]/s', '', $nm);
            
            
            //Check by SKU - it is safer now that we made mandatory in Zoho - ATR 4-30-2020
            $li_sku = $line_item->sku;
            $getItemsId = $zohoBokObj->search_recordsBysku("items","sku",urlencode($li_sku) ,BOOK_ORGID);


            if(empty($getItemsId) and $getItemsId=="" and $getItemsId==0){
            // Create array for new item in items of zoho books
                 $arrCustom[] = array('label'=>"post_id", 'value'=>$line_item->id);
                 
                 $salesItemAry = array(
                    "name" => urlencode($nm),
                    "rate" => ($line_item->subtotal/$line_item->quantity),
                    "sku" => $line_item->sku,
                    "category_id" =>'1655091000001340151',
                    "category_name" => 'KOI',
                    "description" => "Added to Zoho when creating a new Sales Order",
                    "initial_stock" => $line_item->quantity,
                    "initial_stock_rate" => ($line_item->subtotal/$line_item->quantity),
                    "item_type" => "inventory",
                    "product_type" => "goods",
                    "account_id" => '1655091000000000388',
                    "purchase_account_id" => '1655091000000034003',
                    "inventory_account_id" => '1655091000000034001',
                    "purchase_rate" => 1,
                    "purchase_description" => 'Defaulted purchase price to $1',
                    "custom_fields" => $arrCustom
                );
                // Create new item in items of zoho books
                $saleItemID = $zohoBokObj->makeBookRequest($salesItemAry, 'items', BOOK_ORGID,true);
                $getItemsId = $saleItemID->item->item_id;
               file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n New Item Id: ".$getItemsId, FILE_APPEND);

            }
            else{
            //We already have the item in Zoho so we just need to update Zoho Item to reflect final sales price             
               $salesItemAry = array(
                "name" => urlencode($nm),
                "rate" => ($line_item->subtotal/$line_item->quantity),
                "sku" => $line_item->sku
                         
            );
                         
            
                // Update item by item id in items module of zoho books
                $zohoBokObj->updateSalesOrderRequest($salesItemAry, 'items', BOOK_ORGID, true,$getItemsId);
                file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n Updated Item Id: ".$getItemsId, FILE_APPEND);

            }

            // Array to create line items in sales order
            $s=array(
                "name" => urlencode ($nm),
                "item_id" => $getItemsId,
                "item_order" => $line_item->quantity,
                "rate" => ($line_item->subtotal/$line_item->quantity),
                "quantity" => $line_item->quantity,
                "item_total" => $line_item->subtotal
            );

            $itemID[] = $getItemsId;
            $lineItems[] =$s;

            if($wholesale == false){
                $metaData = wc_get_product($line_item->product_id);
                if(count($metaData->category_ids)>0) {
                    foreach ($metaData->category_ids as $key=>$value) {
                        if($term = get_term_by( 'id', $value, 'product_cat' ) ){
                            $catArray[] = $term->name;
                        }
                    }
                    $catMeta = implode(",",$catArray);
                }
                if (strpos($catMeta, 'Wholesale') !== false) {
                    $wholesale = true;
                }
            }
        }

        if(empty($postData->shipping_total)){
            $shipping_charge = 0;
        }else{
            $shipping_charge = $postData->shipping_total + $postData->shipping_tax;
        }
		if(isset($bCerts)){
	    file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n Found Breeder Certificates: ".print_r($lineItems,true), FILE_APPEND);
$lineItems = array_merge($lineItems,$bCertsAry);}
	    file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n Added Breeder Certificates Test: ".print_r($lineItems,true), FILE_APPEND);

        $salesOrderAry = array(
            "customer_id" => $contactID,
            "is_inclusive_tax"=>false,
            "date" => date('Y-m-d', strtotime($postData->date_created)),
            "discount" => abs($postData->discount_total),
            "is_discount_before_tax" => true,
            "discount_type" => "entity_level",
            "line_items" => $lineItems,
            "notes" =>  urlencode($cleanNotes),
            "shipping_charge" => $shipping_charge
        );


        if(!empty($adjustmentArr)){
            $salesOrderAry = array_merge($adjustmentArr,$salesOrderAry);
        }
        // Update sales order in zoho books
        if(!empty($salesordId) and $salesordId!="" and $salesordId>0){
            $salesOrdupdt = $zohoBokObj->updateSalesOrderRequest($salesOrderAry, 'salesorders', BOOK_ORGID, true,$salesordId);
            file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n Updated Sales Order Id: ".$salesordId, FILE_APPEND);
            file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n JSONstring: ".json_encode($salesOrderAry), FILE_APPEND);
            file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n adjustmentArr: ".json_encode($adjustmentArr), FILE_APPEND);


            $saleOrdId = $salesordId;
        }
        else{
            //Create a sales order
            $salesOrderID = $zohoBokObj->makeBookRequest($salesOrderAry,'salesorders',BOOK_ORGID, true,);
            file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n(".$salesOrderID->message.') New Sales Order Id: "'.$salesOrderID->salesorder->salesorder_id, FILE_APPEND);
            file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n JSONstring: ".json_encode($salesOrderAry), FILE_APPEND);
            file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n adjustmentArr: ".json_encode($adjustmentArr), FILE_APPEND);

            $saleOrdId = $salesOrderID->salesorder->salesorder_id;
        }
       

        if ($saleOrdId>0)
        {
         
            // Create invoice from Sales Order
            $invoicesID = $zohoBokObj->convertSalesOrderToInvoice($saleOrdId, true,BOOK_ORGID);
            file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n InvoiceCreated : ".print_r($invoicesID), FILE_APPEND);
            file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n Invoice Id: ".$invoicesID->invoice->invoice_id, FILE_APPEND);
            $invoicesAry = array(
                "custom_fields" => array(
                    0 =>  array(
                          "value" => $postData->id,
                          "label" => "Order Id",
                    ),
                    1 =>  array(
                          "value" => $paymentStatus,
                          "label" => "Woo Order Status",
                    ),
                    2 =>  array(
                          "value" => $postData->payment_method,
                          "label" => "Payment Method",
                    ),
                    3 =>  array(
                          "value" => $postData->number,
                          "label" => "Order Number",
                    ),
                ),
            );
            $arrVal = 4;
            foreach ($postData->meta_data as $key => $value) {
                if($value->key == "best-date-for-receiving"){
                    $invoicesAry["custom_fields"][$arrVal] = array(
                        "value" => $value->value,
                        "label" => "Customer Requested Receiving Date",
                    );
                    $arrVal = 5;
                }
            }

            $saleDev = "OSMT";
           
                    if($wholesale === true){
                        $saleDev = "Wholesale";
                    }
                    else{
                        $saleDev = "OSMT";   
                    }
                    $invoicesAry["custom_fields"][$arrVal] = array(
                        "value" => $saleDev,
                        "label" => "Sales Division",
                    );
                

            // Update invoice to add custom fields
            $invoicesID222 = $zohoBokObj->updateSalesOrderRequest($invoicesAry, 'invoices', BOOK_ORGID, true,$invoicesID->invoice->invoice_id);

            // Update Sales Order to add custom fields
            $salesOrdupdt = $zohoBokObj->updateSalesOrderRequest($invoicesAry, 'salesorders', BOOK_ORGID, true,$saleOrdId);

            if($payment || (($postData->payment_method == "paypal" || $postData->payment_method == "stripe") && $postData->transaction_id != ""))
            {
                $pay_mode = $postData->payment_method == ""?"Cash":ucfirst($postData->payment_method);
                $customerpaymentsAry = array(
                    "customer_id" => $invoicesID->invoice->customer_id,
                    "payment_mode" => $pay_mode,
                    "amount" => $invoicesID->invoice->total,
                    "date" => date('Y-m-d'),
                    "invoices" => array(
                        array(
                            "invoice_id" => $invoicesID->invoice->invoice_id,
                            "amount_applied" => $invoicesID->invoice->total,
                        )
                    ),
                );
                // Create new customer payment
                $customerpaymentsAryRes = $zohoBokObj->makeBookRequest($customerpaymentsAry, 'customerpayments', BOOK_ORGID, true);

                foreach($itemID as $itmid){
                    $itemAuStatus = array(
                        "custom_fields" => array(
                            0 =>  array(
                                  "value" => "Paid Online",
                                  "label" => "Auction Status",
                            )
                        ),
                    );
                    // Update items to add custom fields
                    $updtItem = $zohoBokObj->updateSalesOrderRequest($itemAuStatus, 'items', BOOK_ORGID, true,$itmid);
                }
            }
        }

    } else
    {
        echo 'Error while creating Contact on Book CRM.';
    }
}
else{
    $invoicesAry = array(
        "custom_fields" => array(
            0 =>  array(
                "value" => $paymentStatus,
                "label" => "Woo Order Status",
            ),
            1 =>  array(
                "value" => $postData->number,
                "label" => "Order Number",
            ),
        ),
    );
    $arrVal = 2;
    // add customer requested date to sales order and invoice
    foreach ($postData->meta_data as $key => $value) {
        if($value->key == "best-date-for-receiving"){
            $invoicesAry["custom_fields"][$arrVal] = array(
                "value" => $value->value,
                "label" => "Customer Requested Receiving Date",
            );
            $arrVal = 3;
        }
    }

    $wholesale == false;
    foreach ($postData->line_items as $key => $line_item){
        if($wholesale == false){
            $metaData = wc_get_product($line_item->product_id);
            if(count($metaData->category_ids)>0) {
                foreach ($metaData->category_ids as $key=>$value) {
                    if($term = get_term_by( 'id', $value, 'product_cat' ) ){
                        $catArray[] = $term->name;
                    }
                }
                $catMeta = implode(",",$catArray);
            }
            if (strpos($catMeta, 'Wholesale') !== false) {
                $wholesale = true;
            }
        }
    }
    $saleDev = "OSMT";

            if($wholesale === true){
                $saleDev = "Wholesale";
            }
            else{
                $saleDev = "OSMT";   
            }
            $invoicesAry["custom_fields"][$arrVal] = array(
                "value" => $saleDev,
                "label" => "Sales Division",
            );
     

    // Update invoice to add custom fields
    $invoicesID222 = $zohoBokObj->updateSalesOrderRequest($invoicesAry, 'invoices', BOOK_ORGID, true,$invoiceId);
    file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n Updated Invoice Id: ".$invoiceId, FILE_APPEND);
    if(!empty($salesordId) and $salesordId!="" and $salesordId>0){

        // Update sales order to add custom fields
        $salesOrdupdt = $zohoBokObj->updateSalesOrderRequest($invoicesAry, 'salesorders', BOOK_ORGID, true,$salesordId);
        file_put_contents(date("Y-m-d-").'OrderRequest.txt',"\n\n Updated Sales Orders Id: ".$salesordId, FILE_APPEND);
    }

if($paymentStatus=="Completed" || (($postData->payment_method == "paypal" || $postData->payment_method == "stripe") && $postData->transaction_id != ""))    {
        $dataInvoice=json_decode($zohoBokObj->APICallGet('https://books.zoho.com/api/v3/invoices/'.$invoiceId.'?organization_id='.BOOK_ORGID));
        if(isset($dataInvoice->invoice->invoice_id)){
            if($dataInvoice->invoice->payment_made!="" && $dataInvoice->invoice->payment_made>0){

            }else{
                $pay_mode = $postData->payment_method == ""?"Cash":ucfirst($postData->payment_method);
                $customerpaymentsAry = array(
                    "customer_id" => $dataInvoice->invoice->customer_id,
                    "payment_mode" => $pay_mode,
                    "amount" => $dataInvoice->invoice->total,
                    "date" => date('Y-m-d'),
                    "invoices" => array(
                        array(
                            "invoice_id" => $dataInvoice->invoice->invoice_id,
                            "amount_applied" => $dataInvoice->invoice->total,
                        )
                    ),
                );
                // Create new payment for customer
                $customerpaymentsAryRes = $zohoBokObj->makeBookRequest($customerpaymentsAry, 'customerpayments', BOOK_ORGID, true);
            }
        }

        foreach ($postData->line_items as $key => $line_item)
        {
            $line_item = (object)$line_item;
            $nm = $line_item->name;
            $nm = preg_replace('/[^a-zA-Z0-9_ \/%\[\]\.\(\)%&-]/s', '', $nm);
            // Update itms to add auction status custom field
            //Check by SKU - it is safer now that we made mandatory in Zoho - ATR 4-30-2020
           $li_sku = $line_item->sku;
           $getItemsId = $zohoBokObj->search_recordsBysku("items","sku",urlencode($li_sku) ,BOOK_ORGID);
            if(!empty($getItemsId) and $getItemsId!="" and $getItemsId>0){
                $itemAuStatus = array(
                    "custom_fields" => array(
                        0 =>  array(
                              "value" => "Paid Online",
                              "label" => "Auction Status",
                        )
                    ),
                );
                $updtItem = $zohoBokObj->updateSalesOrderRequest($itemAuStatus, 'items', BOOK_ORGID, true,$getItemsId);
            }
        }
    }
else if($paymentStatus="Pending")
            foreach ($postData->line_items as $key => $line_item)
        {
            
           $li_sku = $line_item->sku;
           $getItemsId = $zohoBokObj->search_recordsBysku("items","sku",urlencode($li_sku) ,BOOK_ORGID);
            if(!empty($getItemsId) and $getItemsId!="" and $getItemsId>0){
                $itemAuStatus = array(
                    "custom_fields" => array(
                        0 =>  array(
                              "value" => "Unpaid Winner",
                              "label" => "Auction Status",
                        )
                    ),
                );
                $updtItem = $zohoBokObj->updateSalesOrderRequest($itemAuStatus, 'items', BOOK_ORGID, true,$getItemsId);
            }
        }
}